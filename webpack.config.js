const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')

module.exports = {
// define entry file and output
    entry: './src/index.js',
    output: {
        path: path.resolve('dist'),
        filename: 'index_bundle.js'
    },
// define babel loader
    module: {
        rules: [
            {test: /\.jsx?$/, loader: 'babel-loader', exclude: /node_modules/}
        ]
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: './src/index.html'
        })
    ]
}
